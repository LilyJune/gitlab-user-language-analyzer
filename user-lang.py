#!/usr/bin/python

import sys
import gitlab

if len(sys.argv) > 1:
  token = sys.argv[1]
  gl = gitlab.Gitlab('https://gitlab.com/', private_token=token, api_version = 4)
  gl.auth()

  username = gl.user.username
  user = gl.users.list(username=username)[0]
  user_projects = user.projects.list(all=True)
  group_projects = []

  for group in gl.groups.list():
    group_projects += group.projects.list()

  projects = user_projects + group_projects
  languages = {}

  for project in projects:
      print('Project: ' + project.name)
      project = gl.projects.get(project.id)
      lang = project.languages()

      for key in lang:
          if key in languages:
            languages[key] += lang[key] / len(projects)
          else:
            languages[key] = lang[key] / len(projects)

  sorted_lang = sorted(languages.items(), key=lambda kv: kv[1])
  sorted_lang.reverse()

  print('\nReport:\n')
  for item in sorted_lang:
      print(str(item[0]) + ': ' + str(round(item[1], 3)) + '%')

else:
    print("Please put your user access token as an argument!")
